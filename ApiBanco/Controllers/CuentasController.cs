﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApiBanco.Models;
using ApiBanco.Controllers.Helpers;

namespace ApiBanco.Controllers
{

    [Route("api/banco/cuentas")]
    public class CuentasController : Controller
    {
        private readonly BancoContext _conext;

        public CuentasController(BancoContext conext)
        {
            _context = context;

            if (_context.Cuentas.Count() == 0)
            {
                List<Cuenta> cuentas = this.CrearCuentas();

                foreach (Cuenta c in cuentas)
                    _context.Cuentas.Add(c);

                _context.SaveChanges();
            }
        }

        private LinkHelper<Cuenta> AddHATEOAS(Cuenta item, string operacion)
        {
            var link = new LinkHelper<Cuenta>(item);

            switch (operacion)
            {
                case "GetCuenta":

                    link.Links.Add(new Link
                    {
                        Href = Url.Link("GetCuenta", new { item.CIF }),
                        Rel = "self",
                        method = "GET"
                    });

                    link.Links.Add(new Link
                    {
                        Href = Url.Link("UpdateCuenta", new { item.CIF }),
                        Rel = "UpdateCuenta",
                        method = "PUT"
                    });


                    link.Links.Add(new Link
                    {
                        Href = Url.Link("DeleteCuenta", new { item.CIF }),
                        Rel = "UpdateCuenta",
                        method = "GET"
                    });

                    break;

                case "UpdateCuenta":

                    link.Links.Add(new Link
                    {
                        Href = Url.Link("GetCuenta", new { item.CIF }),
                        Rel = "GetCuenta",
                        method = "GET"
                    });

                    link.Links.Add(new Link
                    {
                        Href = Url.Link("UpdateCuenta", new { item.CIF }),
                        Rel = "self",
                        method = "PUT"
                    });


                    link.Links.Add(new Link
                    {
                        Href = Url.Link("DeleteCuenta", new { item.CIF }),
                        Rel = "UpdateCuenta",
                        method = "GET"
                    });

                    break;

                case "DeleteCuenta":

                    link.Links.Add(new Link
                    {
                        Href = Url.Link("GetCuenta", new { item.CIF }),
                        Rel = "GetCuenta",
                        method = "GET"
                    });

                    link.Links.Add(new Link
                    {
                        Href = Url.Link("UpdateCuenta", new { item.CIF }),
                        Rel = "UpdateCuenta",
                        method = "PUT"
                    });


                    link.Links.Add(new Link
                    {
                        Href = Url.Link("DeleteCuenta", new { item.CIF }),
                        Rel = "self",
                        method = "GET"
                    });

                    break;
            }

            link.Links.Add(new Link
            {
                Href = Url.Link("GetBanquero", new { item.Id }),
                Rel = "GetBanquero",
                method = "GET"
            });

            return link;
        }

        // GET api/banco/cuentas
        [HttpGet]
        public IEnumerable<LinkHelper<Cuenta>> GetAll()
        {
            //return _context.Cuentas.ToList<Cuenta>();

            IEnumerable<Cuenta> cuentas = _context.Cuentas.ToList<Cuenta>();

            List<LinkHelper<Cuenta>> items = new List<LinkHelper<Cuenta>>();

            foreach (Cuenta c in cuentas)
            {
                items.Add(AddHATEOAS(c, "GetCuenta"));
            }

            return items.ToList<LinkHelper<Cuenta>>();
        }

        // GET api/banco/cuenta/17225879
        [HttpGet("{cif}", Name = "GetCuenta")]
        public IActionResult GetByCIF(string cif)
        {
            var item = _context.Cuentas.FirstOrDefault(t => t.CIF == cif);

            if (item == null)
            {
                return NotFound();
            }
            else
            {
                return new ObjectResult(this.AddHATEOAS(item, "GetCuenta"));
            }
        }

        // POST api/banco/cuenta
        [HttpPost(Name = "CreateCuenta")]
        public IActionResult Post([FromBody] Cuenta value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            _context.Cuentas.Add(value);
            _context.SaveChanges();

            return CreatedAtRoute("GetCuenta", new { cif = value.CIF }, value);
        }

        // PUT api/banco/77
        [HttpPut("{cif}", Name = "UpdateCuenta")]
        public IActionResult Put(string cif, [FromBody] Cuenta value)
        {
            //if (value == null || value.CIF != cif)
            if (value == null)
            {
                return BadRequest();
            }

            var cuenta = _context.Cuentas.FirstOrDefault(t => t.CIF == cif);

            if (cuenta == null)
            {
                return NotFound();
            }

            if (value.CIF != cif)
            {

                _context.Cuentas.Remove(cuenta);
                _context.Cuentas.Add(value);
            }
            else
            {
                cuenta.CIF = value.CIF;
                cuenta.Apellidos = value.Apellidos;
                cuenta.Sector = value.Sector;
                cuenta.UltimaInversionEnMillones = value.UltimaInversionEnMillones;
                cuenta.IdBanquero = value.IdBanquero;

                _context.Cuentas.Update(cuenta);
            }

            _context.SaveChanges();

            return new NoContentResult();
        }

        // PATCH api/banco/17258476/inmobiliario
        [HttpPatch("{cif}", Name = "PatchCuenta")]
        public IActionResult Patch(string cif, [FromBody] PatchValue value)
        {
            var cuenta = _context.Cuentas.FirstOrDefault(t => t.CIF == cif);

            if (cuenta == null)
            {
                return NotFound();
            }

            switch (value.Name)
            {
                case "CIF":
                    cuenta.CIF = value.Value;
                    break;

                case "Apellidos":
                    cuenta.Apellidos = value.Value;
                    break;

                case "Sector":
                    cuenta.Sector = value.Value;
                    break;
                case "UltimaInversionEnMillones":

                    int newInversion = 0;
                    bool response = int.TryParse(value.Value, out newInversion);

                    cuenta.UltimaInversionEnMillones = newInversion;

                    break;

                default:
                    return new NoContentResult();
            }


            _context.Cuentas.Update(cuenta);
            _context.SaveChanges();

            return new NoContentResult();
        }

        // DELETE api/formula1/77
        [HttpDelete("{cif}", Name = "DeleteCuenta")]
        public IActionResult Delete(string cif)
        {
            var cuenta = _context.Cuentas.FirstOrDefault(t => t.CIF == cif);

            if (cuenta == null)
            {
                return NotFound();
            }

            _context.Cuentas.Remove(cuenta);
            _context.SaveChanges();

            return new NoContentResult();
        }

        private List<Cuenta> CrearCuentas()
        {
            List<Cuenta> cuentas = new List<Cuenta>();

            cuentas.Add(new Cuenta("17558256F", "Durasnina Sementov", "inmobiliario", 1));
            cuentas.Add(new Cuenta("17558256F", "Durasnina Sementov", "depósitos", 2));
            cuentas.Add(new Cuenta("20438817B", "Andrea Tumbarello", "bolsa", 2));
            return cuentas;
        }

    }
}

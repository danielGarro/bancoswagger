﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApiBanco.Models;

namespace ApiBanco.Controllers
{
    [Route("http://api/banco/banqueros/")]
    public class BanquerosController : Controller
    {
        private readonly BancoContext _conext;

        public void BanquerosController(BancoContext conext)
        {
            _context = context;

            if (_context.Banqueros.Count() == 0)
            {
                List<Banquero> banqueros = this.CrearBanqueros();

                foreach (Banquero b in banqueros)
                    _context.Banqueros.Add(b);

                _context.SaveChanges();
            }

        }

        // GET api/banco
        [HttpGet]
        public IENumerable<Banqueros> GetAll()
        {
            return _context.Banqueros.ToList<Banquero>();
        }

        public void cuentaCuentas() {
            foreach (var cuenta in _context.Cuentas) {
                foreach(var banquero in _context.Banqueros){/*/adjunto nueva cuenta
                    para saber cuantas tiene ese banquero para ello comparos los ids
                    recogidos de los getters de cada elemento del dbset y al
                    coincidir llamo al método que actualiza una cuenta
                    a ese banquero, ya que actualiza el método con this*/ 
                    if (cuenta.IdBanquero == banquero.Id) {
                        banquero.followCuenta();
                        int millones += cuenta.UltimaInversionEnMillones;
                    }           
                }
            }
        }

        public void recuentaNegocio()
        {
            int millones = 0;
            foreach (var banquero in _context.Banqueros)
            {
                foreach (var cuenta in _context.Cuentas) 
                {/*recorro todas las operaciones asociadas a un banquero
                    cotejando los ids de banquero de los dbset y al terminar
                    de recorre un banquero le digo cuantos millones ha
                    gestionado en inversiones.*/
                    if (cuenta.IdBanquero == banquero.Id)
                    {
                        
                        millones = millones + cuenta.UltimaInversionEnMillones;
                    }
                }
                banquero.recuentaMillones(millones);
            }

        }

        // GET api/banco/137
        [HttpGet("{idBanquero}", Name = "GetBanquero")]
        public IActionResult GetById(int idBanquero)
        {
            var item = _context.Banqueros.FirstOrDefault(t => t.Id == idBanquero);

            if (item == null)
            {
                return NotFound();
            }
            else
            {
                var link = new LinkHelper<Banquero>(item);

                link.Links.Add(new Link
                {
                    Href = Url.Link("GetBanquero", new { item.Id }),
                    Rel = "self",
                    method = "GET"
                });

                switch (item.Id)
                {
                    case 1:

                        link.Links.Add(new Link
                        {//link hateoas a banquero 137
                            Href = Url.Link("GetBanquero", 1),
                            Rel = "http://api/banco/banqueros/1",
                            method = "GET"
                        });

                        return new ObjectResult(link);
                }
            }

            return new ObjectResult(item);
        }

        // POST api/banco
        [HttpPost(Name = "CreateBanquero")]
        public IActionResult Post([FromBody] Banquero value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            _context.Banqueros.Add(value);
            _context.SaveChanges();

            return CreatedAtRoute("GetBanquero", new { idBanquero = value.Id }, value);
        }

        // PUT api/banco/77
        [HttpPut("{idBanquero}", Name = "UpdateBanquero")]
        public IActionResult Put(int idBanquero, [FromBody] Banquero value)
        {
            //if (value == null || value.Id != idBanquero)
            if (value == null)
            {
                return BadRequest();
            }

            var banquero = _context.Banqueros.FirstOrDefault(t => t.Id == idBanquero);

            if (banquero == null)
            {
                return NotFound();
            }

            if (value.Id != idBanquero)
            {

                _context.Banqueros.Remove(banquero);
                _context.Banqueros.Add(value);
            }
            else
            {   //si encuentra id sustituye del objeto recuperado dbset en detrimento propiedad json pasadas por body
                banquero.Id = value.Id;
                banquero.Nombre = value.Nombre;
                banquero.Apellidos = value.Apellidos;
                banquero.Dept = value.Dept;
                banquero.NCuentas = value.NCuentas;
                banquero.MillonesNegocio = value.MillonesNegocio;


                _context.Banqueros.Update(banquero);
            }

            _context.SaveChanges();

            return new NoContentResult();
        }

        // PATCH api/banco/banquero/1
        [HttpPatch("{idBanquero}", Name = "PatchBanquero")]
        public IActionResult Patch(int idBanquero, [FromBody] PatchValue value)
        {
            var banquero = _context.Banqueros.FirstOrDefault(t => t.Id == idBanquero);

            if (banquero == null)
            {
                return NotFound();
            }

            switch (value.Name)
            {
                /*case "Id"://el id es inmodificable, pues habría que verificar que ninguno tiene ese id nuevo

                    int newId = 0;
                    bool response = int.TryParse(value.Value, out newId);

                    banquero.Id = newId;

                    break;*/

                case "Nombre":
                    banquero.Nombre = value.Value;
                    break;

                case "Apellidos":
                    banquero.Apellidos = value.Value;
                    break;

                case "Dept":
                    banquero.Dept = value.Value;
                    break;

                //las cuentas asociadas aumentan cuando aparece cuenta con su id, los millones cuando su cliente invierte

                default:
                    return new NoContentResult();
            }


            _context.Banqueros.Update(banquero);
            _context.SaveChanges();

            return new NoContentResult();
        }

        // DELETE api/formula1/77
        [HttpDelete("{idBanquero}", Name = "DeleteBanquero")]
        public IActionResult Delete(int idBanquero)
        {
            var banquero = _context.Banqueros.FirstOrDefault(t => t.Id == idBanquero);

            if (banquero == null)
            {
                return NotFound();
            }

            _context.Banqueros.Remove(banquero);
            _context.SaveChanges();

            return new NoContentResult();
        }

        private List<Banquero> CrearBanqueros()
        {
            List<Banquero> banqueros = new List<Banquero>();

            banqueros.Add(new Banquero(1, "Juan", "Velázquez", "inmobiliario"));
            banqueros.Add(new Banquero(2, "Elena","Ayuso", "bolsa"));
            banqueros.Add(new Banquero(3,"Angel","Belga","depósitos"));
            banqueros.Add(new Banquero(4,"Daniel","Morlanes","bonos"));
            banqueros.Add(new Banquero(5, "Jared", "Procter", "blockchain"));

            return banqueros;
        }

    }
}

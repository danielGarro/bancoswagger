﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ApiBanco.Models
{
    
    public class BancoContext : DbContext
    {
        public BancoContext(DbContextOptions<BancoContext> options)
          : base(options)
        {
        }

        public DbSet<Banquero> Banqueros { get; set; }

        public DbSet<Cuenta> Cuentas { get; set; }
        

    }
}

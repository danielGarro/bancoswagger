﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ApiBanco.Models
{
    public class Cuenta
    {
        private string cif;
        private string apellidos;
        private string sector;
        private int ultimaInversionEnMillones;
        private int idBanquero;

       

        public Cuenta(string c, string a, string s, int id) :
            this(c, a, s, id, new Random().Next(1, 3))
        {
        }

        public Cuenta(string c,
            string a,
            string s,
            int id,
            int mills)
        {
            this.cif = c;
            this.apellidos = a;
            this.sector = s;
            this.idBanquero = id;
            this.ultimaInversionEnMillones = mills;
        }

        [Key]
        public string CIF { get => cif; set => cif = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Sector { get => sector; set => sector = value; }
        public int UltimaInversionEnMillones { get => ultimaInversionEnMillones; set => ultimaInversionEnMillones = value; }
        public int IdBanquero { get => idBanquero; set => idBanquero = value; }

    }
}

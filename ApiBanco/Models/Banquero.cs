﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ApiBanco.Models
{
    class Banquero
    {
        private int id;
        private string nombre;
        private string apellidos;
        private string dept;
        private int nCuentas;
        private int millonesNegocio;



        public Banquero(int id, string nombre, string apellidos, string dept) :
            this(id, nombre, apellidos, dept, new Random().Next(1, 3), new Random().Next(20, 50))
        {
        }

        private void followCuenta() {
            this.nCuentas++;
        }

        private void recuentaMillones(int millonesCadaCuenta) {
            this.millonesNegocio += millonesCadaCuenta;
        }

        public Banquero(int identificador,
            string n,
            string a,
            string dept,
            int cuentasAsociadas,
            int mills)
        {
            this.id = identificador;
            this.nombre = n;
            this.apellidos = a;
            this.dept = dept;
            this.nCuentas = cuentasAsociadas;
            this.millonesNegocio = mills;
        }

        [Key]
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Dept { get => dept; set => dept = value; }
        public int NCuentas { get => nCuentas; set => nCuentas = value; }
        public int MillonesNegocio { get => millonesNegocio; set => millonesNegocio = value; }
    }
}
